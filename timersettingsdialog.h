#ifndef TIMERSETTINGSDIALOG_H
#define TIMERSETTINGSDIALOG_H

#include <QDialog>

namespace Ui {
class TimerSettingsDialog;
}

class TimerSettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TimerSettingsDialog(QWidget *parent = 0);
    ~TimerSettingsDialog();

private slots:
    void on_buttonBox_accepted();

signals:
    void nameChanged(const QString&);
    void timeChanged(const QTime&);
    void currencyChanged(const QString&);
    void currencyModeChanged(bool);
    void hourlyRateChanged(double);

private:
    Ui::TimerSettingsDialog *ui;
    void setTimeValues(const QTime&);
    QTime getTimeFromValues();
    void showEvent(QShowEvent*);
};

#endif // TIMERSETTINGSDIALOG_H
