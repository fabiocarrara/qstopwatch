#ifndef TIMERCONTROL_H
#define TIMERCONTROL_H

#include "timersettingsdialog.h"
#include <QGroupBox>
#include <QTimer>
#include <QTime>
#include <QMainWindow>

#define DEFAULT_RATE 10.00

namespace Ui {
class TimerControl;
}

class TimerControl : public QGroupBox
{
    Q_OBJECT

public:
    explicit TimerControl(QWidget *parent, QString&, QTime, QString currency = "€", bool currencyState = false, double hourlyRate = DEFAULT_RATE);
    explicit TimerControl(QWidget *parent, QString&);
    ~TimerControl();
    QTime time() const;
    QString currency() const;
    bool currencyState() const;
    double hourlyRate() const;
    QString toString() const;

private:
    uint timeCounter;
    QTime elapsedTime;
    bool running;
    QTimer timer;
    QString _currency;
    bool _currencyState;
    double _hourlyRate;
    TimerSettingsDialog *settingsDialog;
    Ui::TimerControl *ui;

    void updateTimerLabel();

public slots:
    void setName(const QString&);
    void setTime(const QTime&);
    void setCurrency(const QString&);
    void setCurrencyState(bool);
    void setHourlyRate(double);
    void start();
    void pause();
    void showSettingsDialog();
    void remove();
    void toggle();

private slots:
    void tickTimer();
};

#endif // TIMERCONTROL_H
