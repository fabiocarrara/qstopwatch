#include "mainwindow.h"
#include "timercontrol.h"
#include "ui_timercontrol.h"

#include <QMessageBox>

#define mainWindow ((MainWindow*) parent()->parent())

TimerControl::TimerControl(QWidget *parent, QString &title, QTime time, QString currency, bool currencyState, double hourlyRate) :
    QGroupBox(parent),
    elapsedTime(time),
    running(false),
    _currency(currency),
    _currencyState(currencyState),
    _hourlyRate(hourlyRate),
    settingsDialog(Q_NULLPTR),
    ui(new Ui::TimerControl)
{
    ui->setupUi(this);
    setTitle(title);
    updateTimerLabel();
    connect(&timer, SIGNAL(timeout()), this, SLOT(tickTimer()));
}

TimerControl::TimerControl(QWidget *parent, QString &timer) :
    QGroupBox(parent),
    settingsDialog(Q_NULLPTR),
    ui(new Ui::TimerControl)
{
    ui->setupUi(this);
    QStringList values = timer.split("\t");

    setTitle(values.at(0));
    elapsedTime = QTime::fromString(values.at(1), "hh:mm:ss:zzz");
    _currencyState = values.at(2).toInt() == 1;
    _currency = values.at(3);
    _hourlyRate = values.at(4).toDouble();
    running = false;
    updateTimerLabel();
    connect(&this->timer, SIGNAL(timeout()), this, SLOT(tickTimer()));
}

TimerControl::~TimerControl()
{
    delete ui;
}

QTime TimerControl::time() const
{
    return elapsedTime;
}

QString TimerControl::currency() const
{
    return _currency;
}

bool TimerControl::currencyState() const
{
    return _currencyState;
}

double TimerControl::hourlyRate() const
{
    return _hourlyRate;
}

void TimerControl::start()
{
    if(running)
        return;

    timer.start(1000 - elapsedTime.msec());
    ui->toggleTimer->setIcon(QIcon::fromTheme("media-playback-pause"));
    running = true;
}

void TimerControl::pause()
{
    if(!running)
        return;

    elapsedTime = elapsedTime.addMSecs(1000 - timer.remainingTime() - elapsedTime.msec());
    timer.stop();
    ui->toggleTimer->setIcon(QIcon::fromTheme("media-playback-start"));
    running = false;
}

QString TimerControl::toString() const
{
    return title() + "\t" + elapsedTime.toString("hh:mm:ss:zzz") + "\t" + (_currencyState ? "1" : "0") + "\t" + _currency + "\t" + QString::number(_hourlyRate, '.', 2);
}

void TimerControl::setName(const QString &name)
{
    setTitle(name);
    updateTimerLabel();
}

void TimerControl::setTime(const QTime &time)
{
    elapsedTime = time;
    updateTimerLabel();
}

void TimerControl::setCurrency(const QString &currency)
{
    _currency = currency;
    updateTimerLabel();
}

void TimerControl::setCurrencyState(bool state)
{
    _currencyState = state;
    updateTimerLabel();
}

void TimerControl::setHourlyRate(double fee)
{
    _hourlyRate = fee;
    updateTimerLabel();
}

void TimerControl::updateTimerLabel()
{
    QString label;
    if (_currencyState) {
        int secs = QTime(0, 0, 0).secsTo(elapsedTime);
        label = _currency + " " + QString::number((_hourlyRate * secs / 3600), '.', 2);
    }
    else
        label = elapsedTime.toString();

    ui->toggleTimer->setText(label);
}

void TimerControl::showSettingsDialog()
{
    if (settingsDialog == Q_NULLPTR)
        settingsDialog = new TimerSettingsDialog(this);

    settingsDialog->show();
    settingsDialog->exec();
}

void TimerControl::remove()
{
     switch (
            QMessageBox::warning(this,
                         tr("Are you sure?"),
                         tr("Are you sure you want to delete ") + "'" + this->title() + "?'",
                         QMessageBox::Yes,
                         QMessageBox::No)
        ) {

        case QMessageBox::Yes:
            mainWindow->removeTimerFromLayout(this);
            break;

        case QMessageBox::No:
            break;
    }
}

void TimerControl::toggle()
{

    if (!running)
        start();
    else
        pause();
}

void TimerControl::tickTimer()
{
    timer.start(1000);
    elapsedTime = elapsedTime.addSecs(1);
    updateTimerLabel();
}
