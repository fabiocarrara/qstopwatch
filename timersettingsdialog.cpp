#include "timercontrol.h"
#include "timersettingsdialog.h"
#include "ui_timersettingsdialog.h"

#include <QShowEvent>
#include <QTime>

TimerSettingsDialog::TimerSettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TimerSettingsDialog)
{
    ui->setupUi(this);

    TimerControl* par = qobject_cast<TimerControl*>(parent);
    connect(this, SIGNAL(nameChanged(const QString&)), par, SLOT(setName(const QString&)));
    connect(this, SIGNAL(timeChanged(const QTime&)), par, SLOT(setTime(const QTime&)));
    connect(this, SIGNAL(currencyChanged(const QString&)), par, SLOT(setCurrency(const QString&)));
    connect(this, SIGNAL(currencyModeChanged(bool)), par, SLOT(setCurrencyState(bool)));
    connect(this, SIGNAL(hourlyRateChanged(double)), par, SLOT(setHourlyRate(double)));
}

TimerSettingsDialog::~TimerSettingsDialog()
{
    delete ui;
}

void TimerSettingsDialog::setTimeValues(const QTime &t) {
    ui->hourSpinBox->setValue(t.hour());
    ui->minuteSpinBox->setValue(t.minute());
    ui->secondSpinBox->setValue(t.second());
}

QTime TimerSettingsDialog::getTimeFromValues() {
    return  QTime(ui->hourSpinBox->value(),
                  ui->minuteSpinBox->value(),
                  ui->secondSpinBox->value());
}

void TimerSettingsDialog::on_buttonBox_accepted()
{
    emit nameChanged(ui->titleEdit->text());
    emit timeChanged(getTimeFromValues());
    emit currencyChanged(ui->currencyEdit->text());
    emit currencyModeChanged(ui->chargeGroupBox->isChecked());
    emit hourlyRateChanged(ui->chargeSpinBox->value());
}

void TimerSettingsDialog::showEvent(QShowEvent *event)
{
    TimerControl* par = qobject_cast<TimerControl*>(parent());
    ui->titleEdit->setText(par->title());
    setTimeValues(par->time());
    ui->chargeGroupBox->setChecked(par->currencyState());
    ui->chargeSpinBox->setValue(par->hourlyRate());
    ui->titleEdit->setFocus();

    event->accept();
}
