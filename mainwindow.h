#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "timercontrol.h"
#include <QCloseEvent>
#include <QList>
#include <QMainWindow>
#include <QMenu>
#include <QSettings>
#include <QSystemTrayIcon>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void addTimerToLayout(TimerControl*);
    void removeTimerFromLayout(TimerControl*);

private:
    Ui::MainWindow *ui;
    QSettings settings;
    QList<TimerControl*> timers;
    QSystemTrayIcon trayIcon;
    QMenu trayIconMenu;

    TimerControl* addTimer(QString, QTime time);
    TimerControl* addTimer(QString);
    bool exportTimers(QString&);
    bool importTimers(QString&);
    void deleteTimers();
    void closeEvent(QCloseEvent*);
    void readSettings();
    void writeSettings();
    bool checkTitle(QString);
    void createTrayIcon();

private slots:
    void trayIconActivated(QSystemTrayIcon::ActivationReason reason);
    void closeWindow();
    void fixSize();
    void newTimer();
    bool on_actionImport_triggered();
    bool on_actionExport_triggered();
    void on_actionStartAll_triggered();
    void on_actionPauseAll_triggered();
    void on_actionSwitchAll_triggered();
    void on_actionRemoveAll_triggered();
    void on_actionAbout_triggered();
};

#endif // MAINWINDOW_H
