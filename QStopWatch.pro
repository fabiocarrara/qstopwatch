#-------------------------------------------------
#
# Project created by QtCreator 2013-10-28T11:16:50
#
#-------------------------------------------------

QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QStopWatch
TEMPLATE = app


SOURCES	+= \
	main.cpp \
	mainwindow.cpp \
	timercontrol.cpp \
	timersettingsdialog.cpp

HEADERS += \
	mainwindow.h \
	timercontrol.h \
	timersettingsdialog.h

RESOURCES += \
	resources.qrc

FORMS += \
	mainwindow.ui \
	timercontrol.ui \
	timersettingsdialog.ui

OTHER_FILES += \
	README.md
