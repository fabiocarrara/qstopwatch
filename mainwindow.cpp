#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QFile>
#include <QIcon>
#include <QInputDialog>
#include <QLayout>
#include <QMessageBox>
#include <QTextStream>
#include <QTime>

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    settings("QStopWatch", "QStopWatch")

{
    ui->setupUi(this);
    readSettings();
    layout()->setSizeConstraint(QLayout::SetFixedSize);

    createTrayIcon();
}

TimerControl* MainWindow::addTimer(QString timerTitle, QTime time)
{
    TimerControl *tc = new TimerControl(this, timerTitle, time);
    addTimerToLayout(tc);
    return tc;
}

TimerControl* MainWindow::addTimer(QString timer)
{
    TimerControl *tc = new TimerControl(this, timer);
    addTimerToLayout(tc);
    return tc;
}

bool MainWindow::exportTimers(QString &filename)
{
    QFile f(filename);
    if (!f.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;

    QTextStream fout(&f);

    foreach (TimerControl* tc, timers)
        fout << tc->toString() << endl;

    f.close();
    return true;
}

bool MainWindow::importTimers(QString &filename)
{
    QFile f(filename);
    if (!f.open(QIODevice::ReadOnly | QIODevice::Text))
        return false;

    QTextStream fin(&f);

    while (!fin.atEnd()) {
        QString timer = fin.readLine();
        QString timerTitle = timer.split('\t').at(0);

        if (fin.status() != 0)
            break;

        addTimer(timer);
    }

    return true;
}

void MainWindow::addTimerToLayout(TimerControl* tc)
{
    timers.push_back(tc);
    ui->timersLayout->addWidget(tc);
}

void MainWindow::removeTimerFromLayout(TimerControl* tc)
{
    timers.removeOne(tc);
    ui->timersLayout->removeWidget(tc);
    delete(tc);

    QTimer::singleShot(0, this, SLOT(fixSize()));
}

void MainWindow::deleteTimers() {
    foreach(TimerControl* tc, timers)
        removeTimerFromLayout(tc);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if(event->spontaneous()) {
        setVisible(false);
        event->ignore();
        return;
    }

    event->accept();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::readSettings()
{
    settings.beginGroup("timers");

    foreach (QString key, settings.childKeys())
        addTimer(settings.value(key).toString());

    settings.endGroup();
}

void MainWindow::writeSettings()
{
    int i = 0;

    settings.beginGroup("timers");
    settings.remove(""); // delete all timers in group
    foreach(TimerControl* tc, timers) {
        settings.setValue(QString::number(i++), tc->toString());
    }
    settings.endGroup();
}

bool MainWindow::checkTitle(QString timerTitle)
{
    if(timerTitle.isEmpty() || timerTitle.contains('\t'))
        return false;

    foreach(TimerControl* tc, timers)
        if(QString::compare(timerTitle, tc->title()) == 0)
            return false;

    return true;
}

void MainWindow::trayIconActivated(QSystemTrayIcon::ActivationReason reason)
{
    if(reason == QSystemTrayIcon::Trigger)
        setVisible(!isVisible());
}

void MainWindow::closeWindow()
{
    on_actionPauseAll_triggered();
    writeSettings();
    trayIcon.hide();
    close();
}

void MainWindow::createTrayIcon()
{
    connect(&trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(trayIconActivated(QSystemTrayIcon::ActivationReason)));

    trayIconMenu.addSection("QStopWatch");
//    trayIconMenu.addAction(QIcon::fromTheme("document-save"), "&Export", this, SLOT(on_actionExport_triggered()));
//    trayIconMenu.addAction(QIcon::fromTheme("document-open"), "&Import", this, SLOT(on_actionImport_triggered()));
//    trayIconMenu.addSeparator();
    trayIconMenu.addAction(QIcon::fromTheme("media-playback-start"), "&Start all", this, SLOT(on_actionStartAll_triggered()));
    trayIconMenu.addAction(QIcon::fromTheme("media-playback-pause"), "&Pause all", this, SLOT(on_actionPauseAll_triggered()));
//    trayIconMenu.addAction(QIcon::fromTheme("emblem-favorite"), "&Switch all", this, SLOT(on_actionSwitchAll_triggered())); doesn't work in this way, it should check SwitchAll action
//    trayIconMenu.addAction(QIcon::fromTheme("edit-delete"), "&Remove all", this, SLOT(on_actionRemoveAll_triggered()));
    trayIconMenu.addSeparator();
    trayIconMenu.addAction(QIcon::fromTheme("help-about"), "&About", this, SLOT(on_actionAbout_triggered()));
    trayIconMenu.addSeparator();
    trayIconMenu.addAction(QIcon::fromTheme("application-exit"), "&Exit", this, SLOT(closeWindow()));
    trayIcon.setIcon(QIcon(":/res/icons/icon.png"));
    trayIcon.setContextMenu(&trayIconMenu);
    trayIcon.show();
}

void MainWindow::fixSize()
{
    adjustSize();
}

void MainWindow::newTimer()
{
    TimerControl *tc = addTimer("Untitled Timer", QTime(0,0,0));
    tc->showSettingsDialog();
}

bool MainWindow::on_actionExport_triggered()
{
    QString filename = QFileDialog::getSaveFileName(this,
                                                    tr("Save Timers"), // caption
                                                    QString(), // default dir
                                                    tr("Timers (*.tmr)") // default filter
                                                    );
    if (filename == Q_NULLPTR)
        return false;
    if (!filename.endsWith(".tmr"))
        filename += ".tmr";

    return exportTimers(filename);
}

bool MainWindow::on_actionImport_triggered()
{
    QString filename = QFileDialog::getOpenFileName(this,
                                                    tr("Open Timers"),
                                                    QString(),
                                                    tr("Timers (*.tmr)")
                                                    );
    return importTimers(filename);
}

void MainWindow::on_actionStartAll_triggered()
{
    foreach(TimerControl *tc, timers)
        tc->start();
}

void MainWindow::on_actionPauseAll_triggered()
{
    foreach(TimerControl *tc, timers)
        tc->pause();
}

void MainWindow::on_actionSwitchAll_triggered()
{
    bool val = ui->actionSwitchAll->isChecked();

    foreach (TimerControl* tc, timers)
            tc->setCurrencyState(val);
}

void MainWindow::on_actionRemoveAll_triggered()
{
    if(QMessageBox::warning(this,
                            tr("Are you sure?"),
                            tr("Are you sure you want to delete all timers?"),
                            QMessageBox::Ok,
                            QMessageBox::Cancel) == QMessageBox::Ok)
        deleteTimers();
}

void MainWindow::on_actionAbout_triggered()
{
    QString aboutText =
            "<pre style='font-family:sans-serif'>Authors:\n\n"
            "Fabio Carrara:\t<a href='mailto:fabio.blackdragon@gmail.com'>fabio.blackdragon@gmail.com</a>\n"
            "Daniele Formichelli:\t<a href='mailto:daniele.formichelli@gmail.com'>daniele.formichelli@gmail.com</a></pre>";
    QMessageBox::about(this, "About QStopWatch", aboutText);
}
